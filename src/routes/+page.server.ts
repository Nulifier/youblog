import { getLatestPosts, getPosts } from "$lib/server/posts";

export async function load() {
	return {
		posts: getLatestPosts(5)
	};
}
