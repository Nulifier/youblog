import {
	PostType,
	getPost,
	getPosts,
	type MarkdownPost
} from "$lib/server/posts";

export async function entries() {
	const posts = await getPosts();
	return posts
		.filter((post) => post.type === PostType.Markdown)
		.map((post) => ({ slug: post.meta.slug }));
}

export async function load({ params }) {
	const post = getPost(params.slug);
	if (post.type !== PostType.Markdown) {
		throw new Error("Only markdown posts can be rendered by the slug route");
	}
	return {
		slug: params.slug,
		post: post as MarkdownPost
	};
}
