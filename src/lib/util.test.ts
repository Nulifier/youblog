import { expect, test } from "vitest";
import { isDateValid, splitWords, stripHtmlTags } from "./util";

test("isValidDate()", () => {
	expect(isDateValid(new Date("2000-03-13"))).toBe(true);
	expect(isDateValid(new Date("last tuesday"))).toBe(false);
});

test("stripHtmlTags()", () => {
	expect(stripHtmlTags("Simple test")).toBe("Simple test");
	expect(stripHtmlTags("<p>Simple paragraph</p>")).toBe("Simple paragraph");
});

test("splitWords()", () => {
	expect(splitWords("Basic text")).toEqual(["Basic", "text"]);
	expect(splitWords("Multiple  spaces test")).toEqual([
		"Multiple",
		"spaces",
		"test"
	]);
});
