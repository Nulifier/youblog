const MONTHS_LONG = [
	"January",
	"February",
	"March",
	"April",
	"May",
	"June",
	"July",
	"August",
	"September",
	"October",
	"November",
	"December"
];

export function formatDate(date: Date) {
	return `${
		MONTHS_LONG[date.getMonth()]
	} ${date.getDate()}, ${date.getFullYear()}`;
}
