import { isDateValid, splitWords, stripHtmlTags } from "$lib/util";
import { error } from "@sveltejs/kit";
import frontMatter from "front-matter";
import { marked } from "marked";
import { parse as parsePath } from "node:path";
import slugify from "slugify";

export enum PostType {
	Markdown,
	Svelte
}

export interface Post {
	type: PostType;
	meta: PostMetadata;
}

export interface MarkdownPost extends Post {
	type: PostType.Markdown;
	body: string;
}

export interface SveltePost extends Post {
	type: PostType.Svelte;
}

export interface PostMetadata {
	slug: string;
	title: string;
	posted: Date;
	summary: string;
	tags: string[];
}

export function loadPostsSync(): Post[] {
	const markdownPosts = Object.entries(
		import.meta.glob("$posts/*.md", { eager: true, as: "raw" })
	).map(([filename, post]) => loadMarkdownPost(filename, post));

	const sveltePosts: SveltePost[] = Object.entries(
		import.meta.glob(
			["$routes/posts/*/+page.svelte", "!$routes/posts/[slug]/+page.svelte"],
			{ eager: true }
		)
	).map(([filename, post]) => {
		if (!post || typeof post !== "object" || !("meta" in post)) {
			throw new Error("Svelte posts must export a meta object from the module");
		}
		return {
			type: PostType.Svelte,
			meta: post.meta as PostMetadata
		};
	});

	return [...markdownPosts, ...sveltePosts];
}

export function loadMarkdownPost(
	filename: string,
	postFile: string
): MarkdownPost {
	const { attributes, body: bodySource } = frontMatter<{ [key: string]: any }>(
		postFile
	);

	if (attributes["slug"] && !(typeof attributes["slug"] !== "string")) {
		throw new Error("If slug is provided, it must be a string");
	}
	const title = attributes["title"];
	if (!title || typeof title !== "string") {
		throw new Error("Post title must be a string");
	}
	const posted = new Date(attributes["posted"]);
	if (!attributes["posted"] || !isDateValid(posted)) {
		throw new Error("Posted date must be a valid date (YYYY-MM-DD)");
	}
	const tags = attributes["tags"] || [];
	if (!Array.isArray(tags) || !tags.every((tag) => typeof tag === "string")) {
		throw new Error("If tags are provided, they must be an array of strings");
	}

	const body = marked.parse(bodySource);

	return {
		type: PostType.Markdown,
		meta: {
			slug: slugify(parsePath(filename).name),
			title,
			posted,
			summary: createPostSummary(body),
			tags: tags as string[]
		},
		body
	};
}

export function createPostSummary(postBody: string, maxWords = 70) {
	const plainBody = stripHtmlTags(postBody);
	const words = splitWords(plainBody).slice(0, maxWords);
	return words.join(" ");
}

const posts = loadPostsSync();

export function getPosts() {
	return posts;
}

/**
 * Gets the latest set of posts, sorted from newest to oldest.
 * @param count The number of posts to return.
 */
export function getLatestPosts(count: number) {
	const postList = posts.toSorted(
		(a, b) => b.meta.posted.getTime() - a.meta.posted.getTime()
	);
	postList.splice(count);
	return postList;
}

export function getPost(slug: string) {
	const post = posts.find((post) => post.meta.slug === slug);
	if (!post) {
		throw error(404, `Post '${slug}' not found`);
	}

	return post;
}
