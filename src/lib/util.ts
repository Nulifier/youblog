import { stripHtml } from "string-strip-html";

export function isDateValid(d: Date) {
	return d.toString() !== "Invalid Date";
}

export function stripHtmlTags(source: string) {
	return stripHtml(source).result;
}

export function splitWords(text: string) {
	return text.split(/\s+/);
}
