---
title: Spa Controller - Part 1
posted: 2019-09-13
tags:
  - spa
---

Last summer I was finally convinced to buy a hot tub. After all the work
required to build a concrete pad and wire it up, I finally got to relax in it
and enjoy my hard work.

Until Winter.

Two things happened: it was way colder going to and from the tub and my
utility bill started to climb. Since I pretty much only used it on the weekend I
lowered the temperature during the week. But this involved having to remember to
turn down the temperature and going out a few hours before I wanted to use the
tub on Friday to turn back up the temperature.

This wasn't going to work for me as I have the memory of a goldfish so I decided
to do what every self-respecting Electrical Engineer would do: over-complicate
the heck out of it with electronics and automation.

## The Plan

What I wanted was basically a Wifi-enabled thermostat that I could setup a basic
schedule and adjust it without going outside. Many of these systems already
existed but they were for tubs much newer than mine. After searching for an
off-the-shelf solution I resolved myself to having to build something.

Now the problem was figuring out a way to inferface with the brains of the tub.
After much research I came across a blog entry from someone who seemed to be
wrestling with the same issue. The key was their description of the RJ45
connector and what each of the pins did. I poked around inside the control panel
of my tub and found the same RJ45 connector.

ADD PIC OF CONTROL BOARD

I did a basic sanity check using a multimeter and found most of the pins were
low voltage except for pins 1 & 2 which were a bit weird and averaged out at
around 6-7V. Confident that I wouldn't fry it, I hooked up the signals to a
logic and tried to piece together what the signals were, by testing various
display modes and button presses.

## The Data

Once I had the data captured and I was back inside, I found a period burst of
what looked like data on pins 5 and 6 at 60 Hz. Pin 6 must be the clock as pin 5
only changed sometimes and it looked like the shift register data that the blog
entry I referenced mentioned, only a lot more data than they had.

![Logic analyzer output](/images/spa/logic-analyzer-total.png "Logic analyzer output")

I took all the test data I had an placed it into an excel sheet. In each update
there were 76 bits of data and I wanted to figure out what each one meant. Since
the blog post had some information about how the numbers and letters on the 7
segment display were done, I first started looking for those patterns.

The first bit was always zero so I ignored that one. I grouped the next bits
into four sets of 7 bits to represent each of the digits as they lined up with
the abcdefg layout from this table on Wikipedia. Through trial and error and
planning several tests that would only change one part of the display, I mapped
out all of the display bits. There were still several bits interspersed that had
no meaning but I assumed they might have some meaning on other models.

## Button Presses

There were six bits at the end that were a bit odd, they had no meaning by
themselves but the clock pulses were much longer for these bits. When looking at
the data, pin 3 seemed to have pulses that matched up with these clock pulses.
When I was pressing buttons, I noticed that different patterns of bits would be
sent on pin 3. Again, pressing all the buttons and recording the results, I
figured out all the button combinations:

| Button    | Bit 0 | Bit 1 | Bit 2 | Bit 3 | Bit 4 | Bit 5 | Bits [3-5] |
| --------- | ----- | ----- | ----- | ----- | ----- | ----- | ---------- |
| Nothing   | 1     | 1     | 0     | 0     | 0     | 0     |            |
| Hash (#)  | 1     | 1     | 1     | 0     | 0     | 0     | 0          |
| Pump 1    | 1     | 1     | 1     | 0     | 0     | 1     | 1          |
| Pump 2    | 1     | 1     | 1     | 0     | 1     | 0     | 2          |
| Light     | 1     | 1     | 1     | 0     | 1     | 1     | 3          |
| Clock     | 1     | 1     | 1     | 1     | 0     | 0     | 4          |
| Turbo     | 1     | 1     | 1     | 1     | 0     | 1     | 5          |
| Temp Up   | 1     | 1     | 1     | 1     | 1     | 0     | 6          |
| Temp Down | 1     | 1     | 1     | 1     | 1     | 1     | 7          |

It looked bit bits 0 and 1 were for some sort of sense function. They might have
something to do with detecting if the the two interface panels are attached
(further research required). Bit 2 is true if the button is pressed. Bits 3-5
encode which button is being pressed. We should be able to drive this signal
high to simulate a button press as I suspect there is a pulldown transistor in
the control board as it only turns pulls low about halfway through the data
stream.

So after all this, I built an excel sheet that can decode the state of the hot
tub and made a little simulated display (you can check it out HERE).

ADD LINK TO SHEET

Now the next step is to build a prototype using an Arduino to see if our theory
works!
